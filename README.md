# Vacuum Hot Cathode Gauge MKS

EPICS module to read/write data for MKS vacuum hot cathode gauge.

IOC does not directly communicate with the gauge, the gauge is actully connected to a vacuum controller. IOC communicated to the controller and controller provides the gauge data. Controllers can have multiple gauges in deifferent channels.


The modules arrangement should be something like:
```
IOC-|
    |-vac-ctrl-mks946
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|      |-vac-gauge-mks-vgc (Cold cathode gauge)
	|
	|-vac-ctrl-mks946
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|	   |-vac-mfc-mks-gv50a (Mass flow controller)
```

This allows EPICS modules for gauges to be re-used and any combination of controller and gauges to be built from existing modules.

