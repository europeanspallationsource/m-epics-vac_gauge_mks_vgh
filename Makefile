include ${EPICS_ENV_PATH}/module.Makefile

DOC       = README.md
MISC      = protocol/vac-gauge-mks-vgh.proto
STARTUPS  = startup/vac-gauge-mks-vgh.cmd

USR_DEPENDENCIES = asyn,4.21
USR_DEPENDENCIES += streamdevice,2.7.1
